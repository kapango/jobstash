package com.kapango.jobstash.matchers;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobTitles {

    private final Set<String> list;

    public JobTitles(final ObjectMapper mapper) throws IOException, URISyntaxException {
        final URL jobTitlesUrl = getClass().getClassLoader().getResource("json/entities/job_titles.json");
        list  = mapper.readValue(new File(jobTitlesUrl.toURI()), new TypeReference<Set<String>>() {});
    }
}