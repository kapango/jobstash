package com.kapango.jobstash.matchers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.kapango.jobstash.constants.JobTypes;

@Component
public class JobTypeMatcher {

    private static final List<String> FULLTIME = List.of("full time", "ft", "full-time", "fulltime");
    private static final List<String> PARTTIME = List.of("part time", "pt", "part-time", "parttime");
    private static final List<String> CONTRACT = List.of("contractor", "contract", "consultant", "agency");
    private static final List<String> TEMPORARY = List.of("temp", "temporary");

    public List<String> match(String text) {

        List<String> matches = new ArrayList<>();
        if (FULLTIME.stream().anyMatch(text::contains)) {
            matches.add(JobTypes.PARTTIME);
        }
        if (PARTTIME.stream().anyMatch(text::contains)) {
            matches.add(JobTypes.PARTTIME);
        }
        if (CONTRACT.stream().anyMatch(text::contains)) {
            matches.add(JobTypes.CONTRACT);
        }
        if (TEMPORARY.stream().anyMatch(text::contains)) {
            matches.add(JobTypes.TEMPORARY);
        }
        return matches;
    }
}
