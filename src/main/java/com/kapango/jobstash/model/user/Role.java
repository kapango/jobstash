package com.kapango.jobstash.model.user;

import java.util.Arrays;

public enum Role {

    USER("User"),
    ADMIN("Admin");

    private String roleName;

    Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public static Role getRoleByName(final String name) {
        return Arrays.stream(Role.values()).filter(r -> r.getRoleName().equals(name)).findFirst().orElse(null);
    }

}
