package com.kapango.jobstash.model.jobqueue;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "job_queue")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobQueue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "hackernews_topic_id")
    private String hackernewsTopicId;

    @Column(name = "hackernews_post_id")
    private String hackernewsPostId;

    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "origin")
    private String origin;

    @Column(name = "state")
    private String state;

    @Column(name = "user")
    private String user;

    @Column(name = "text")
    private String text;
}
