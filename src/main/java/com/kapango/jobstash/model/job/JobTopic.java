package com.kapango.jobstash.model.job;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "job_topic")
@Data
@NoArgsConstructor
public class JobTopic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "topic_id")
    private String topicId;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @Column(name = "source")
    private String source;
}
