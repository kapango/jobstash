package com.kapango.jobstash.model.organisation;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.kapango.jobstash.constants.OrganisationState;
import com.kapango.jobstash.model.Sector;
import com.kapango.jobstash.model.StringListConverter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "organisation")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Organisation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "subdivision")
    private String subdivision;

    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "description_short")
    private String descriptionShort;

    @Column(name = "description")
    private String description;

    @Column(name = "url")
    private String url;

    @Column(name = "type")
    private String type;

    @Column(name = "updated")
    private LocalDateTime updated;

    @ManyToMany
    @JoinTable(
            name = "organisation_sectors",
            joinColumns = { @JoinColumn(name = "sector_id") },
            inverseJoinColumns = { @JoinColumn(name = "organisation_id") }
    )
    private List<Sector> sectors;

    @Column(name = "country")
    private String country;

    @Column(name = "region")
    private String region;

    @Column(name = "full_location")
    private String location;

    @Column(name = "trustpilot_url")
    private String trustpilotUrl;

    @Column(name = "trustpilot_score")
    private Double trustpilotScore;

    @Column(name = "glassdoor_url")
    private String glassdoorUrl;

    @Column(name = "glassdoor_score")
    private Double glassdoorScore;

    @Column(name = "wiki_url")
    private String wikiUrl;

    @Column(name = "allows_remote")
    private String allowsRemote;

    @Column(name = "relocation_support")
    private Boolean relocationSupport;

    @Column(name = "visa_sponsor")
    private Boolean visaSponsor;

    @Column(name = "city")
    private String city;

    @Column(name = "ycombinator_intake")
    private String ycombinatorIntake;

    @Column(name = "ycombinator_url")
    private String ycombinatorUrl;

    @Column(name = "crunchbase_url")
    private String crunchbaseUrl;

    @Column(name = "linkedin_url")
    private String linkedinUrl;

    @Column(name = "facebook_url")
    private String facebookUrl;

    @Column(name = "twitter_url")
    private String twitterUrl;

    @Column(name = "instagram_url")
    private String instagramUrl;

    @Column(name = "state")
    private String state = OrganisationState.CREATED;

    @Column(name = "employees")
    private Integer employees;

    @Column(name = "year_founded")
    private Integer yearFounded;

    @Column(name = "image")
    private String image;

    @Column(name = "image_alt")
    private String imageAlt;

    @Convert(converter = StringListConverter.class)
    @Column(name = "other_links")
    private List<String> otherLinks;

}
