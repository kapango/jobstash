package com.kapango.jobstash.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.kapango.jobstash.model.user.Role;

@Converter
public class RoleConverter implements AttributeConverter<Set<Role>, String> {
    private static final String SPLIT_CHAR = ";";

    @Override
    public String convertToDatabaseColumn(final Set<Role> roles) {
        final List<String> roleList = roles.stream().map(Role::getRoleName).collect(Collectors.toList());
        return String.join(SPLIT_CHAR, roleList);
    }

    @Override
    public Set<Role> convertToEntityAttribute(final String string) {

        if (string == null) {
            return new HashSet<>();
        }
        return Arrays.stream(string.split(SPLIT_CHAR)).map(Role::getRoleByName).collect(Collectors.toSet());
    }
}
