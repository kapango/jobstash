package com.kapango.jobstash.publisher;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class HackerNewsPostCache {

  private final Set<String> postCache;

  public HackerNewsPostCache() {
    this.postCache = new HashSet<>();
  }

  @Bean
  public Set<String> getPostCache() {
    return postCache;
  }

}
