package com.kapango.jobstash.bootstrap;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableSet;
import com.kapango.jobstash.model.user.JobstashUser;
import com.kapango.jobstash.model.user.Role;
import com.kapango.jobstash.repository.user.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j(topic = "USER_BOOTSTRAPPER")
public class UserBootstrapper {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserBootstrapper(final UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        loadUsers();
    }

    private void loadUsers() {
        log.info("Loading users");

        this.repository.save(JobstashUser.builder()
                .email("bruce@test.com")
                .name("Bruce")
                .password(passwordEncoder.encode("test"))
                        .roles(ImmutableSet.of(Role.ADMIN, Role.USER))
                .build());
    }
}
