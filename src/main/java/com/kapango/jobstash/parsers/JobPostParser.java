package com.kapango.jobstash.parsers;

public interface JobPostParser {
    JobPostDetails parse(String jobPost) throws JobPostParserException;
}
