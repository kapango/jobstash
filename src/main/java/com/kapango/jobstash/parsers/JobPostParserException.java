package com.kapango.jobstash.parsers;

public class JobPostParserException extends Exception {

    public JobPostParserException(String s) {
        super(s);
    }
}
