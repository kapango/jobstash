package com.kapango.jobstash.parsers;

import java.net.URI;
import java.util.List;

import com.kapango.jobstash.model.token.ProgrammingFramework;
import com.kapango.jobstash.model.token.ProgrammingLanguage;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobPostDetails {

    private String companyName;
    private List<String> lines;
    private String jobType;
    private List<ProgrammingLanguage> programmingLanguages;
    private List<ProgrammingFramework> programmingFrameworks;
    private List<String> technologies;
    private List<String> roles;
    private List<URI> links;
    private URI organisationUrl;
    private boolean allowsRemote;
    private List<String> remoteRestrictedAreas;
}
