package com.kapango.jobstash.service.job;

import com.kapango.jobstash.api.job.JobResponse;
import com.kapango.jobstash.client.hackernews.HackerNewsPostResponse;

class JobMapper {

  private JobMapper() {
  }

  /**
   * Maps a JobClient response into a
   *
   * @param response
   * @return JobResponse
   */
  static JobResponse map(final HackerNewsPostResponse response) {

    return JobResponse.builder()
        .id(response.getId())
        .by(response.getBy())
        .time(response.getTime())
        .title(response.getTitle())
        .descendants(response.getDescendants())
        .kids(response.getKids())
        .score(response.getScore())
        .text(response.getText())
        .build();
  }
}
