package com.kapango.jobstash.security;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.kapango.jobstash.model.user.JobstashUser;
import com.kapango.jobstash.repository.user.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<JobstashUser> result = userRepository.findFirstByEmail(username);

        if (result.isPresent()) {
            final JobstashUser jobstashUser = result.get();
            return new User(jobstashUser.getEmail(), jobstashUser.getPassword(), getAuthorities(jobstashUser));
        } else {
            throw new UsernameNotFoundException("No user present with username: " + username);
        }
    }

    private static List<GrantedAuthority> getAuthorities(JobstashUser jobstashUser) {
        return jobstashUser.getRoles().stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role.getRoleName()))
                .collect(Collectors.toList());
    }
}
