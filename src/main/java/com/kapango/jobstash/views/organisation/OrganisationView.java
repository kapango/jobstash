package com.kapango.jobstash.views.organisation;

import java.time.Duration;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.kapango.jobstash.model.organisation.Organisation;
import com.kapango.jobstash.service.organisation.OrganisationService;
import com.kapango.jobstash.views.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("Organisation")
@Route(value = "list/:organisationID?/:action?(edit)", layout = MainLayout.class)
@Uses(Icon.class)
public class OrganisationView extends Div implements BeforeEnterObserver {

    private final String ORGANISATION_ID = "organisationID";
    private final String ORGANISATION_EDIT_ROUTE_TEMPLATE = "list/%d/edit";

    private Grid<Organisation> grid = new Grid<>(Organisation.class, false);

    private TextField id;
    private TextField name;
    private TextField subdivision;
    private DateTimePicker created;
    private TextField descriptionShort;
    private TextField description;
    private TextField url;
    private TextField type;
    private DateTimePicker updated;
    private TextField sectors;
    private TextField country;
    private TextField region;
    private TextField location;
    private TextField city;
    private TextField trustpilotUrl;
    private TextField trustpilotScore;
    private TextField glassdoorUrl;
    private TextField glassdoorScore;
    private TextField wikiUrl;
    private Checkbox allowsRemote;
    private Checkbox relocationSupport;
    private Checkbox visaSponsor;
    private TextField ycombinatorIntake;
    private TextField ycombinatorUrl;
    private TextField crunchbaseUrl;
    private TextField linkedinUrl;
    private TextField facebookUrl;
    private TextField twitterUrl;
    private TextField instagramUrl;
    private TextField state;
    private TextField employees;
    private TextField yearFounded;
    private TextField image;
    private TextField imageAlt;
    private TextField otherLinks;

    private Button cancel = new Button("Cancel");
    private Button save = new Button("Save");

    private BeanValidationBinder<Organisation> binder;

    private Organisation organisation;

    private OrganisationService organisationService;

    public OrganisationView(@Autowired OrganisationService organisationService) {
        this.organisationService = organisationService;
        addClassNames("list-view", "flex", "flex-col", "h-full");
        // Create UI
        SplitLayout splitLayout = new SplitLayout();
        splitLayout.setSizeFull();

        createGridLayout(splitLayout);
        createEditorLayout(splitLayout);

        add(splitLayout);

        // Configure Grid
        grid.addColumn("id").setAutoWidth(true);
        grid.addColumn("name").setAutoWidth(true);
        grid.addColumn("subdivision").setAutoWidth(true);
        grid.addColumn("created").setAutoWidth(true);
        grid.addColumn("descriptionShort").setAutoWidth(true);
        grid.addColumn("description").setAutoWidth(true);
        grid.addColumn("url").setAutoWidth(true);
        grid.addColumn("type").setAutoWidth(true);
        grid.addColumn("updated").setAutoWidth(true);
        grid.addColumn("sectors").setAutoWidth(true);
        grid.addColumn("country").setAutoWidth(true);
        grid.addColumn("region").setAutoWidth(true);
        grid.addColumn("location").setAutoWidth(true);
        grid.addColumn("city").setAutoWidth(true);
        grid.addColumn("trustpilotUrl").setAutoWidth(true);
        grid.addColumn("trustpilotScore").setAutoWidth(true);
        grid.addColumn("glassdoorUrl").setAutoWidth(true);
        grid.addColumn("glassdoorScore").setAutoWidth(true);
        grid.addColumn("wikiUrl").setAutoWidth(true);
        TemplateRenderer<Organisation> allowsRemoteRenderer = TemplateRenderer.<Organisation>of(
                "<iron-icon hidden='[[!item.allowsRemote]]' icon='vaadin:check' style='width: var(--lumo-icon-size-s); height: var(--lumo-icon-size-s); color: var(--lumo-primary-text-color);'></iron-icon><iron-icon hidden='[[item.allowsRemote]]' icon='vaadin:minus' style='width: var(--lumo-icon-size-s); height: var(--lumo-icon-size-s); color: var(--lumo-disabled-text-color);'></iron-icon>")
                .withProperty("allowsRemote", Organisation::getAllowsRemote);
        grid.addColumn(allowsRemoteRenderer).setHeader("Allows Remote").setAutoWidth(true);

        TemplateRenderer<Organisation> relocationSupportRenderer = TemplateRenderer.<Organisation>of(
                "<iron-icon hidden='[[!item.relocationSupport]]' icon='vaadin:check' style='width: var(--lumo-icon-size-s); height: var(--lumo-icon-size-s); color: var(--lumo-primary-text-color);'></iron-icon><iron-icon hidden='[[item.relocationSupport]]' icon='vaadin:minus' style='width: var(--lumo-icon-size-s); height: var(--lumo-icon-size-s); color: var(--lumo-disabled-text-color);'></iron-icon>")
                .withProperty("relocationSupport", Organisation::getRelocationSupport);
        grid.addColumn(relocationSupportRenderer).setHeader("Relocation Support").setAutoWidth(true);

        TemplateRenderer<Organisation> visaSponsorRenderer = TemplateRenderer.<Organisation>of(
                "<iron-icon hidden='[[!item.visaSponsor]]' icon='vaadin:check' style='width: var(--lumo-icon-size-s); height: var(--lumo-icon-size-s); color: var(--lumo-primary-text-color);'></iron-icon><iron-icon hidden='[[item.visaSponsor]]' icon='vaadin:minus' style='width: var(--lumo-icon-size-s); height: var(--lumo-icon-size-s); color: var(--lumo-disabled-text-color);'></iron-icon>")
                .withProperty("visaSponsor", Organisation::getVisaSponsor);
        grid.addColumn(visaSponsorRenderer).setHeader("Visa Sponsor").setAutoWidth(true);

        grid.addColumn("ycombinatorIntake").setAutoWidth(true);
        grid.addColumn("ycombinatorUrl").setAutoWidth(true);
        grid.addColumn("crunchbaseUrl").setAutoWidth(true);
        grid.addColumn("linkedinUrl").setAutoWidth(true);
        grid.addColumn("facebookUrl").setAutoWidth(true);
        grid.addColumn("twitterUrl").setAutoWidth(true);
        grid.addColumn("instagramUrl").setAutoWidth(true);
        grid.addColumn("state").setAutoWidth(true);
        grid.addColumn("employees").setAutoWidth(true);
        grid.addColumn("yearFounded").setAutoWidth(true);
        grid.addColumn("image").setAutoWidth(true);
        grid.addColumn("imageAlt").setAutoWidth(true);
        grid.addColumn("otherLinks").setAutoWidth(true);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setHeightFull();

        // when a row is selected or deselected, populate form
        grid.asSingleSelect().addValueChangeListener(event -> {
            if (event.getValue() != null) {
                UI.getCurrent().navigate(String.format(ORGANISATION_EDIT_ROUTE_TEMPLATE, event.getValue().getId()));
            } else {
                clearForm();
                UI.getCurrent().navigate(OrganisationView.class);
            }
        });

        // Configure Form
        binder = new BeanValidationBinder<>(Organisation.class);

        // Bind fields. This where you'd define e.g. validation rules
        binder.forField(id).withConverter(new StringToIntegerConverter("Only numbers are allowed")).bind("id");
        binder.forField(employees).withConverter(new StringToIntegerConverter("Only numbers are allowed"))
                .bind("employees");
        binder.forField(yearFounded).withConverter(new StringToIntegerConverter("Only numbers are allowed"))
                .bind("yearFounded");

        binder.bindInstanceFields(this);

        cancel.addClickListener(e -> {
            clearForm();
            refreshGrid();
        });

        save.addClickListener(e -> {
            try {
                if (this.organisation == null) {
                    this.organisation = new Organisation();
                }
                binder.writeBean(this.organisation);

                organisationService.save(this.organisation);
                clearForm();
                refreshGrid();
                Notification.show("Organisation details stored.");
                UI.getCurrent().navigate(OrganisationView.class);
            } catch (ValidationException validationException) {
                Notification.show("An exception happened while trying to store the organisation details.");
            }
        });

    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Optional<Integer> organisationId = event.getRouteParameters().getInteger(ORGANISATION_ID);
        if (organisationId.isPresent()) {
            Optional<Organisation> organisationFromBackend = organisationService.findById(organisationId.get());
            if (organisationFromBackend.isPresent()) {
                populateForm(organisationFromBackend.get());
            } else {
                Notification.show(
                        String.format("The requested organisation was not found, ID = %d", organisationId.get()), 3000,
                        Notification.Position.BOTTOM_START);
                // when a row is selected but the data is no longer available,
                // refresh grid
                refreshGrid();
                event.forwardTo(OrganisationView.class);
            }
        }
    }

    private void createEditorLayout(SplitLayout splitLayout) {
        Div editorLayoutDiv = new Div();
        editorLayoutDiv.setClassName("flex flex-col");
        editorLayoutDiv.setWidth("400px");

        Div editorDiv = new Div();
        editorDiv.setClassName("p-l flex-grow");
        editorLayoutDiv.add(editorDiv);

        FormLayout formLayout = new FormLayout();
        id = new TextField("Id");
        name = new TextField("Name");
        subdivision = new TextField("Subdivision");
        created = new DateTimePicker("Created");
        created.setStep(Duration.ofSeconds(1));
        descriptionShort = new TextField("Description Short");
        description = new TextField("Description");
        url = new TextField("Url");
        type = new TextField("Type");
        updated = new DateTimePicker("Updated");
        updated.setStep(Duration.ofSeconds(1));
        sectors = new TextField("Sectors");
        country = new TextField("Country");
        region = new TextField("Region");
        location = new TextField("Location");
        city = new TextField("City");
        trustpilotUrl = new TextField("Trustpilot Url");
        trustpilotScore = new TextField("Trustpilot Score");
        glassdoorUrl = new TextField("Glassdoor Url");
        glassdoorScore = new TextField("Glassdoor Score");
        wikiUrl = new TextField("Wiki Url");
        allowsRemote = new Checkbox("Allows Remote");
        allowsRemote.getStyle().set("padding-top", "var(--lumo-space-m)");
        relocationSupport = new Checkbox("Relocation Support");
        relocationSupport.getStyle().set("padding-top", "var(--lumo-space-m)");
        visaSponsor = new Checkbox("Visa Sponsor");
        visaSponsor.getStyle().set("padding-top", "var(--lumo-space-m)");
        ycombinatorIntake = new TextField("Ycombinator Intake");
        ycombinatorUrl = new TextField("Ycombinator Url");
        crunchbaseUrl = new TextField("Crunchbase Url");
        linkedinUrl = new TextField("Linkedin Url");
        facebookUrl = new TextField("Facebook Url");
        twitterUrl = new TextField("Twitter Url");
        instagramUrl = new TextField("Instagram Url");
        state = new TextField("State");
        employees = new TextField("Employees");
        yearFounded = new TextField("Year Founded");
        image = new TextField("Image");
        imageAlt = new TextField("Image Alt");
        otherLinks = new TextField("Other Links");
        Component[] fields = new Component[]{id, name, subdivision, created, descriptionShort, description, url, type,
                updated, sectors, country, region, location, city, trustpilotUrl, trustpilotScore, glassdoorUrl,
                glassdoorScore, wikiUrl, allowsRemote, relocationSupport, visaSponsor, ycombinatorIntake,
                ycombinatorUrl, crunchbaseUrl, linkedinUrl, facebookUrl, twitterUrl, instagramUrl, state, employees,
                yearFounded, image, imageAlt, otherLinks};

        for (Component field : fields) {
            ((HasStyle) field).addClassName("full-width");
        }
        formLayout.add(fields);
        editorDiv.add(formLayout);
        createButtonLayout(editorLayoutDiv);

        splitLayout.addToSecondary(editorLayoutDiv);
    }

    private void createButtonLayout(Div editorLayoutDiv) {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setClassName("w-full flex-wrap bg-contrast-5 py-s px-l");
        buttonLayout.setSpacing(true);
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        buttonLayout.add(save, cancel);
        editorLayoutDiv.add(buttonLayout);
    }

    private void createGridLayout(SplitLayout splitLayout) {
        Div wrapper = new Div();
        wrapper.setId("grid-wrapper");
        wrapper.setWidthFull();
        splitLayout.addToPrimary(wrapper);
        wrapper.add(grid);
    }

    private void refreshGrid() {
        grid.select(null);
        grid.getDataProvider().refreshAll();
    }

    private void clearForm() {
        populateForm(null);
    }

    private void populateForm(Organisation value) {
        this.organisation = value;
        binder.readBean(this.organisation);

    }
}
