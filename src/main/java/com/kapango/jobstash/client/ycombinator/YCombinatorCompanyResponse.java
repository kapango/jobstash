package com.kapango.jobstash.client.ycombinator;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class YCombinatorCompanyResponse {

    private String ycombinatorUrl;
    private String companyUrl;
    private String companyName;
    private String image;
    private String imageAlt;
    private Integer yearFounded;
    private Integer employees;
    private List<String> sectors;
    private String location;
    private String descriptionShort;
    private String descriptionLong;
    private String facebookUrl;
    private String twitterUrl;
    private String crunchbaseUrl;
    private String linkedInUrl;
    private String ycombinatorIntake;
    private String stage;

}
