package com.kapango.jobstash.client.hackernews;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/v0")
public interface HackerNewsHttpClient {

  @GET
  @Path("/jobstories.json")
  @Produces({MediaType.APPLICATION_JSON})
  List<String> getLatestJobs();

  @GET
  @Path("/item/{postId}.json")
  @Produces({MediaType.APPLICATION_JSON})
  HackerNewsPostResponse getPostDetails(@PathParam("postId") String postId);

  @GET
  @Path("/user/{userId}.json")
  @Produces({MediaType.APPLICATION_JSON})
  HackerNewsUserResponse getUserDetails(@PathParam("userId") String userId);

}