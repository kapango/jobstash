package com.kapango.jobstash.client.hackernews;

import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.stereotype.Component;

@Component
public class HackerNewsHttpClientImpl implements HackerNewsHttpClient {

  private static final String HN_URL = "https://hacker-news.firebaseio.com";

  private HackerNewsHttpClient client;

  public HackerNewsHttpClientImpl() {
    WebTarget target = ClientBuilder.newClient().target(HN_URL);
    ResteasyWebTarget rtarget = (ResteasyWebTarget)target;
    client = rtarget.proxy(HackerNewsHttpClient.class);
  }

  @Override
  public List<String> getLatestJobs() {
    return client.getLatestJobs();
  }

  @Override
  public HackerNewsPostResponse getPostDetails(final String postId) {
    return client.getPostDetails(postId);
  }

  @Override
  public HackerNewsUserResponse getUserDetails(final String userId) {
    return client.getUserDetails(userId);
  }
}
