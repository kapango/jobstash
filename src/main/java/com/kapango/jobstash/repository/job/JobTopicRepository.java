package com.kapango.jobstash.repository.job;

import com.kapango.jobstash.model.job.JobTopic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JobTopicRepository extends CrudRepository<JobTopic, Integer> {

    @Query(value = "SELECT * FROM job_topic ORDER BY id DESC LIMIT 1", nativeQuery = true)
    Optional<JobTopic> getLatestJobTopic();
}
