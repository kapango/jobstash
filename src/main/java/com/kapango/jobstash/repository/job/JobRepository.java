package com.kapango.jobstash.repository.job;

import com.kapango.jobstash.model.job.Job;
import com.kapango.jobstash.model.jobqueue.JobQueue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Transactional
@Repository
public interface JobRepository extends CrudRepository<Job, Integer> {

    @Query(value = "SELECT * FROM job_post ORDER BY id DESC LIMIT 1", nativeQuery = true)
    Optional<JobQueue> getLatestJobPost();

    @Query(value = "SELECT job_post_id FROM job_post WHERE job_topic_id = :jobTopicId", nativeQuery = true)
    Set<String> getAllJobPostIds(@Param("jobTopicId") String jobTopicId);

    @Query(value = "SELECT * FROM job_post WHERE state = :state ORDER BY created ASC LIMIT 1", nativeQuery = true)
    Optional<JobQueue> findOneByState(@Param("state") String state);

}
