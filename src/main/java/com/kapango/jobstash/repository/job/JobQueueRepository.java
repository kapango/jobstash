package com.kapango.jobstash.repository.job;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kapango.jobstash.model.jobqueue.JobQueue;

@Transactional
@Repository
public interface JobQueueRepository extends CrudRepository<JobQueue, Integer> {

    @Query(value = "SELECT * FROM job_queue ORDER BY id DESC LIMIT 1", nativeQuery = true)
    Optional<JobQueue> getLatestJobPost();

    @Query(value = "SELECT hackernews_post_id FROM job_queue WHERE hackernews_topic_id = :hackernewsTopicId", nativeQuery = true)
    Set<String> getAllHackerNewsJobsInQueue(@Param("hackernewsTopicId") String hackernewsTopicId);

    @Query(value = "SELECT * FROM job_queue WHERE state = :state ORDER BY created ASC LIMIT 1", nativeQuery = true)
    Optional<JobQueue> findOneByState(@Param("state") String state);

}
