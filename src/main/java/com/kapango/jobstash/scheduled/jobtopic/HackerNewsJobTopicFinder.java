package com.kapango.jobstash.scheduled.jobtopic;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.kapango.jobstash.client.hackernews.HackerNewsHttpClient;
import com.kapango.jobstash.model.job.JobTopic;
import com.kapango.jobstash.repository.job.JobTopicRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Business logic for controlling where to fetch job posts and how to handle the data returned.
 * <p>
 * This is a scheduled task managed by Quartz scheduler in Spring boot
 */
@Component
@Slf4j(topic = "HACKER_NEWS_JOB_INGESTER")
@DisallowConcurrentExecution
public class HackerNewsJobTopicFinder extends QuartzJobBean {

    public static final String JOB_NAME = "HACKER_NEWS_JOB_INGESTER";
    public static final String SOURCE = "HACKERNEWS";

    private final HackerNewsHttpClient client;
    private final JobTopicRepository jobTopicRepository;

    public HackerNewsJobTopicFinder(final HackerNewsHttpClient hackerNewsClient,
                                    final JobTopicRepository jobTopicRepository) {
        this.client = hackerNewsClient;
        this.jobTopicRepository = jobTopicRepository;
    }

    @Override
    protected void executeInternal(@NotNull final JobExecutionContext jobExecutionContext) {
        final String recentJobTopicId = client.getUserDetails("whoishiring").getSubmitted().get(0).toString();

        final Optional<JobTopic> jobTopic = jobTopicRepository.getLatestJobTopic();

        if (!jobTopic.isPresent() || !jobTopic.get().getTopicId().equals(recentJobTopicId)) {

            log.info("Creating new Job topic with ID {}", recentJobTopicId);

            final JobTopic newJobTopic = new JobTopic();
            newJobTopic.setTopicId(recentJobTopicId);
            newJobTopic.setTimestamp(LocalDateTime.now());
            newJobTopic.setSource(SOURCE);

            jobTopicRepository.save(newJobTopic);

        } else {
            log.info("Hackernews Job topic with ID {} has already been discovered", recentJobTopicId);
        }
    }
}
