package com.kapango.jobstash.scheduled;

import javax.annotation.PostConstruct;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kapango.jobstash.scheduled.jobqueue.HackerNewsJobQueue;
import com.kapango.jobstash.scheduled.jobtopic.HackerNewsJobTopicFinder;
import com.kapango.jobstash.scheduled.organisation.GlassDoorEnricher;
import com.kapango.jobstash.scheduled.organisation.OrganisationEnricher;

@Component
public class ScheduledJobManager {

    private final Scheduler scheduler;

    @Autowired
    public ScheduledJobManager(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @PostConstruct
    public void scheduler() throws SchedulerException {

        final SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInHours(24).repeatForever();


        //Job Queue Populators
        JobDetail hackernewsJobQueue = buildJob("hackernewsJobQueue", "jobqueue", HackerNewsJobQueue.class);


        //Data enrichers
        JobDetail orgnisationEnricher = buildJob("organisationEnricher", "organisation", OrganisationEnricher.class);
        JobDetail glassdoorEnricher = buildJob("glassdoorEnricher", "organisation", GlassDoorEnricher.class);

        //Job Queue transformers
        JobDetail hackernewsJobQueueTransformer = buildJob("hackernewsJobQueueTransformer", "jobqueue", HackernewsJobQueueTransformer.class);

        //Misc
        JobDetail hackerNewsJobTopic = buildJob("hackerNewsJobTopicFinder", "hackernews", HackerNewsJobTopicFinder.class);


        scheduler.scheduleJob(hackerNewsJobTopic, TriggerBuilder
                .newTrigger()
                .forJob(hackerNewsJobTopic)
                .withIdentity("hackerNewsJobTopicFinder", "hackernews")
                .withSchedule(scheduleBuilder.withIntervalInHours(24))
                .build());

        scheduler.scheduleJob(orgnisationEnricher, TriggerBuilder
                .newTrigger()
                .forJob(orgnisationEnricher)
                .withIdentity("organisationEnricher", "organisation")
                .withSchedule(scheduleBuilder.withIntervalInMinutes(1))
                .build());

//        scheduler.scheduleJob(glassdoorEnricher, TriggerBuilder
//                .newTrigger()
//                .forJob(glassdoorEnricher)
//                .withIdentity("glassdoorEnricher", "organisation")
//                .withSchedule(scheduleBuilder.withIntervalInMinutes(5))
//                .build());

        scheduler.scheduleJob(hackernewsJobQueue, TriggerBuilder
                .newTrigger()
                .forJob(hackernewsJobQueue)
                .withIdentity("hackernewsJobQueue", "jobqueue")
                .withSchedule(scheduleBuilder.withIntervalInMinutes(10))
                .build());

        scheduler.scheduleJob(hackernewsJobQueueTransformer, TriggerBuilder
                .newTrigger()
                .forJob(hackernewsJobQueueTransformer)
                .withIdentity("hackernewsJobQueueTransformer", "jobqueue")
                .withSchedule(scheduleBuilder.withIntervalInSeconds(2))
                .build());

    }

    private JobDetail buildJob(String jobName, String jobGroup, Class<? extends Job> jobType) {
        return JobBuilder.newJob(jobType)
                .withIdentity(jobName, jobGroup)
                .storeDurably(true)
                .build();
    }
}
