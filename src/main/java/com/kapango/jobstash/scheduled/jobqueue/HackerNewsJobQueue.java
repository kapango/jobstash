package com.kapango.jobstash.scheduled.jobqueue;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.jsoup.Jsoup;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.kapango.jobstash.client.hackernews.HackerNewsHttpClient;
import com.kapango.jobstash.client.hackernews.HackerNewsPostResponse;
import com.kapango.jobstash.model.jobqueue.JobQueue;
import com.kapango.jobstash.model.job.JobTopic;
import com.kapango.jobstash.repository.job.JobQueueRepository;
import com.kapango.jobstash.repository.job.JobTopicRepository;
import com.kapango.jobstash.constants.JobQueueState;

import lombok.extern.slf4j.Slf4j;

/**
 * Searches the most recent Hacker News Job Topic for jobs to input into the Job queue
 */
@Component
@Slf4j(topic = "HACKERNEWS_JOB_QUEUE")
@DisallowConcurrentExecution
public class HackerNewsJobQueue extends QuartzJobBean {

    private final HackerNewsHttpClient client;
    private final Set<String> postCache;
    private final JobTopicRepository jobTopicRepository;
    private final JobQueueRepository jobQueueRepository;
    private Optional<JobTopic> latestJobTopic;

    @Autowired
    public HackerNewsJobQueue(final HackerNewsHttpClient hackerNewsClient,
                              final Set<String> postCache, JobTopicRepository jobTopicRepository,
                              final JobQueueRepository jobQueueRepository) {
        this.client = hackerNewsClient;
        this.postCache = postCache;
        this.jobTopicRepository = jobTopicRepository;
        this.jobQueueRepository = jobQueueRepository;
        latestJobTopic = jobTopicRepository.getLatestJobTopic();
        loadCacheFromDb();
    }

    private void loadCacheFromDb() {

        if (latestJobTopic.isPresent()) {

            final Set<String> savedJobs = jobQueueRepository.getAllHackerNewsJobsInQueue(latestJobTopic.get().getTopicId());
            postCache.clear();
            postCache.addAll(savedJobs);
        }
    }

    @Override
    protected void executeInternal(@NotNull final JobExecutionContext jobExecutionContext) throws JobExecutionException {

        if (latestJobTopic.isEmpty()) {
            log.info("No job topics currently found or discovered, exiting");
        } else {

            final String currentTopicId = latestJobTopic.get().getTopicId();

            log.info("Fetching job posts in Hacker news job topic {}", currentTopicId);

            final HackerNewsPostResponse response = client.getPostDetails(currentTopicId);

            log.info("There are {} job posts", response.getKids().size());

            response.getKids().removeAll(postCache);

            log.info("There are {} job post still to be added to the database", response.getKids().size());

            response.getKids().forEach(jobCommentId -> {

                log.info("Processing job comment - " + jobCommentId);

                final HackerNewsPostResponse jobComment = client.getPostDetails(jobCommentId);

                if (jobComment != null) {

                    if (!jobComment.isDead() && !jobComment.isDeleted()) {

                        final JobQueue jobQueue = JobQueue.builder()
                                .hackernewsTopicId(currentTopicId)
                                .hackernewsPostId(jobCommentId)
                                .state(JobQueueState.CREATED)
                                .created(LocalDateTime.now())
                                .state(JobQueueState.CREATED)
                                .user(jobComment.getBy())
                                .text(jobComment.getText())
                                .build();

                        jobQueueRepository.save(jobQueue);

                        log.info(Jsoup.parse(jobComment.getText()).text());
                    } else {

                        log.info("Job post is dead, recording it to the database");

                        final JobQueue jobQueue = JobQueue.builder()
                                .hackernewsTopicId(currentTopicId)
                                .hackernewsPostId(jobCommentId)
                                .state(JobQueueState.INVALID)
                                .created(LocalDateTime.now())
                                .state(JobQueueState.CREATED)
                                .user(jobComment.getBy())
                                .text(jobComment.getText())
                                .build();

                        jobQueueRepository.save(jobQueue);
                    }
                    postCache.add(jobCommentId);
                } else {
                    log.info("Job is invalid or null ");
                    postCache.add(jobCommentId);
                }
            });
        }
    }
}
