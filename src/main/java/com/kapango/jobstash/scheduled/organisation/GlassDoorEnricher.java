package com.kapango.jobstash.scheduled.organisation;

import javax.validation.constraints.NotNull;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.kapango.jobstash.client.glassdoor.GlassDoorClientException;
import com.kapango.jobstash.client.glassdoor.GlassDoorNotFoundException;
import com.kapango.jobstash.client.glassdoor.GlassdoorHttpClientImpl;
import com.kapango.jobstash.repository.organisation.OrganisationRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Looks for Glassdoor profile for organisation
 */
@Component
@Slf4j(topic = "GLASSDOOR_ENRICHER")
@DisallowConcurrentExecution
public class GlassDoorEnricher extends QuartzJobBean {

    private final OrganisationRepository repository;
    private final GlassdoorHttpClientImpl glassdoorHttpClient;

    @Autowired
    public GlassDoorEnricher(final OrganisationRepository repository,
                             final GlassdoorHttpClientImpl glassdoorHttpClient) {
        this.repository = repository;
        this.glassdoorHttpClient = glassdoorHttpClient;
    }

    /**
     * Find companies that don't have glassdoor URLs, if no Glassdoor profile
     * <p>
     * Then url is set to "NOT_FOUND"
     */
    @Override
    protected void executeInternal(@NotNull final JobExecutionContext jobExecutionContext) {
        var result = repository.findFirstByGlassdoorUrlIsNull();

        if (result.isPresent()) {
            var organisation = result.get();
            log.info("Found an Organisation: {} that needs Glassdoor scoring", organisation.getName());

            try {
                var glassdoorCompany = glassdoorHttpClient.getGlassdoorCompany(organisation.getName());

                organisation.setGlassdoorScore(glassdoorCompany.getScore());
                organisation.setGlassdoorUrl(glassdoorCompany.getUrl());
                repository.save(organisation);
            } catch (GlassDoorNotFoundException e) {
                log.warn("Could not find a Glassdoor profile for {}", organisation.getName());
                organisation.setGlassdoorUrl("NOT_FOUND");
                repository.save(organisation);

            } catch (GlassDoorClientException e) {
                log.error("Failed to get Glassdoor information, perhaps page has changed?}", e);
            }

        } else {
            log.info("No Organisations left to update Glassdoor for");
        }
    }
}
