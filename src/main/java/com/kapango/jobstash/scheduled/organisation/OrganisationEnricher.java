package com.kapango.jobstash.scheduled.organisation;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.kapango.jobstash.client.ycombinator.YCombinatorClient;
import com.kapango.jobstash.constants.OrganisationState;
import com.kapango.jobstash.repository.organisation.OrganisationRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Populated Organisation with useful information
 */
@Component
@Slf4j(topic = "ORGANISATION_ENRICHER")
@DisallowConcurrentExecution
public class OrganisationEnricher extends QuartzJobBean {

    private final YCombinatorClient yCombinatorClient;
    private final OrganisationRepository repository;

    @Autowired
    public OrganisationEnricher(final YCombinatorClient yCombinatorClient, final OrganisationRepository repository) {
        this.yCombinatorClient = yCombinatorClient;
        this.repository = repository;
    }


    @Override
    protected void executeInternal(@NotNull final JobExecutionContext jobExecutionContext) {
        var result = repository.findFirstByState(OrganisationState.CREATED);

        if (result.isPresent()) {
            var organisation = result.get();
            log.info("Found an Organisation: {} that needs enrichment", organisation.getName());

            //see if they are Ycombinator company
            if (organisation.getYcombinatorUrl() != null) {
                log.info("{} is a YCombinator company", organisation.getName());

                try {
                    log.info("{} has a YCombinator URL {}", organisation.getName(), organisation.getYcombinatorUrl());
                    var response = yCombinatorClient.fetchCompanyByUrl(organisation.getYcombinatorUrl());
                    organisation.setUrl(response.getCompanyUrl());
                    organisation.setDescription(response.getDescriptionLong());
                    organisation.setDescriptionShort(response.getDescriptionShort());
                    organisation.setCrunchbaseUrl(response.getCrunchbaseUrl());
                    organisation.setFacebookUrl(response.getFacebookUrl());
                    organisation.setTwitterUrl(response.getTwitterUrl());
                    organisation.setLinkedinUrl(response.getLinkedInUrl());
                    organisation.setEmployees(response.getEmployees());
                    organisation.setYearFounded(response.getYearFounded());
                    organisation.setImage(response.getImage());
                    organisation.setImageAlt(response.getImageAlt());
                    organisation.setLocation(response.getLocation());
                    organisation.setState(OrganisationState.ENRICHED);
                    organisation.setUpdated(LocalDateTime.now());
                    log.info("{} has been enriched with data from YCombinator", organisation.getName());

                    repository.save(organisation);
                } catch (IOException e) {
                    log.error("Unable to extract information for {} from YCombinator", organisation.getName(), e);
                }
            }
        } else {
            log.info("No new Organisations to be enriched");
        }
    }
}
