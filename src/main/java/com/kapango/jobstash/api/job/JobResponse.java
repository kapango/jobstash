package com.kapango.jobstash.api.job;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobResponse implements Serializable {

  public String by;
  public Integer descendants;
  public Integer id;
  @Builder.Default
  public Set<String> kids = new HashSet<>();
  public Integer score;
  public Integer time;
  public String title;
  public String type;
  public String url;
  public String text;

}
