-- liquibase formatted sql
-- changeset bruce.taylor:add-many-to-many-job-programming-languages

create table job_programming_languages
(
    id int,
    programming_language_id int not null,
    job_id int not null,
    constraint job_programming_languages_job_id_fk
        foreign key (job_id) references job (id),
    constraint job_programming_languages_programming_language_id_fk
        foreign key (programming_language_id) references programming_language (id)
);

create unique index job_programming_languages_id_uindex
    on job_programming_languages (id);

alter table job_programming_languages
    add constraint job_programming_languages_pk
        primary key (id);

alter table job_programming_languages modify id int auto_increment;

create index job_programming_languages_programming_language_id
    on job_programming_languages (programming_language_id);

create index job_programming_languages_job_id
    on job_programming_languages (job_id);