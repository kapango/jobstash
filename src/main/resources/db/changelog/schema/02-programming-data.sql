-- liquibase formatted sql
-- changeset bruce.taylor:add-programming-language-table

create table programming_language
(
    id       int,
    name     varchar(50)   not null,
    alias    varchar(50)   null,
    wiki_url varchar(2083) null
);

create unique index programming_languages_id_uindex
    on programming_language (id);

create index programming_languages_name_index
    on programming_language (name);

alter table programming_language
    add constraint programming_languages_pk
        primary key (id);

alter table programming_language
    modify id int auto_increment;

-- changeset bruce.taylor:rename-job-tables
rename table token_job_industries to job_industries;
rename table token_job_location to job_location;
rename table token_job_sector to job_sector;
rename table token_job_title to job_title;
rename table token_job_types to job_types;
