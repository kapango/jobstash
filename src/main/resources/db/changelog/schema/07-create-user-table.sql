-- liquibase formatted sql
-- changeset bruce.taylor:create-user-table

create table user
(
    id       int,
    name     varchar(150)               not null,
    email    varchar(255)               not null,
    password varchar(150)               not null,
    roles    varchar(80) DEFAULT 'User' not null
);

create unique index user_email_uindex
    on user (email);

create unique index user_id_uindex
    on user (id);

alter table user
    add constraint user_pk
        primary key (id);

alter table user
    modify id int auto_increment;

