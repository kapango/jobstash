-- liquibase formatted sql
-- changeset bruce.taylor:add-additional-links-column

alter table organisation
    add other_links text null;
