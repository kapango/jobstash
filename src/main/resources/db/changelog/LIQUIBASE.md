# Liquibase Guide

Changesets must be in SQL format.

Filenaming convention should be as follows:  `{number}-{reference}-{description}.sql`

e.g.
> 01-BAU-123-add-foo-table.sql

> 02-CCC-52-delete-customer-index.sql

Numbers should follow a logical/ordinal approach i.e. 1,2,3...101

