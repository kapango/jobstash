package com.kapango.jobstash.client.ycombinator;


import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class YCombinatorClientTest {

    private YCombinatorClient client;

    @Before
    public void setUp() throws Exception {
        client = new YCombinatorClient();
    }

    @Test
    public void fetchCompany() throws IOException {
        YCombinatorCompanyResponse company = client.fetchCompanyByName("airbnb");
        assertThat(company.getCompanyName()).isEqualTo("Airbnb");
    }

    @Test
    public void fetchCompanyByUrl() throws IOException {
        YCombinatorCompanyResponse company = client.fetchCompanyByUrl("https://www.ycombinator.com/companies/mobydish");
        assertThat(company.getCompanyName()).isEqualTo("Mobydish");
        assertThat(company.getYearFounded()).isNull();
        assertThat(company.getLocation()).isEqualTo("San Francisco");
        assertThat(company.getEmployees()).isEqualTo(10);
        assertThat(company.getYcombinatorIntake()).isEqualTo("W15");
        assertThat(new File("img/organisation/Mobydish.png")).isFile();
    }
}