package com.kapango.jobstash.matchers;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.kapango.jobstash.model.token.ProgrammingLanguage;
import com.kapango.jobstash.repository.token.ProgrammingLanguageRepository;

public class ProgrammingLanguagesMatcherTest {

    private ProgrammingLanguagesMatcher matcher;

    private final ProgrammingLanguage javascript = ProgrammingLanguage.builder().name("Javascript").alias("Java-script").build();
    private final ProgrammingLanguage typescript = ProgrammingLanguage.builder().name("Typescript").alias("Type-script").build();
    private final ProgrammingLanguage java = ProgrammingLanguage.builder().name("Java").alias("").build();
    private final ProgrammingLanguage python = ProgrammingLanguage.builder().name("Python").alias("").build();

    @Before
    public void setUp() {

        ProgrammingLanguageRepository repository = mock(ProgrammingLanguageRepository.class);

        when(repository.findAll()).thenReturn(
                ImmutableList.of(
                        javascript,
                        typescript,
                        java,
                        python
                )
        );
        matcher = new ProgrammingLanguagesMatcher(repository);
    }

    @Test
    public void match_whenSingleWordThatMatches_ThenReturnCorrectMatch() {
        final String alias = "JAVA";
        final List<ProgrammingLanguage> match = matcher.match(List.of(alias));
        assertThat(match.size()).isEqualTo(1);
        assertThat(match.get(0)).isEqualTo(java);
    }

    @Test
    public void match_whenSingleWordThatMatchesAlias_ThenReturnCorrectMatch() {
        final String alias = "typescript";
        final List<ProgrammingLanguage> match = matcher.match(List.of(alias));
        assertThat(match.size()).isEqualTo(1);
        assertThat(match.get(0)).isEqualTo(typescript);
    }

    @Test
    public void match_whenMultipleWordsToMatch_ThenReturnCorrectMatches() {
        final List<String> words = ImmutableList.of("java", "java-script", "bob", "foo", "PYTHON");
        final List<ProgrammingLanguage> matches = matcher.match(words);
        assertThat(matches.size()).isEqualTo(3);
        assertThat(matches).contains(java, javascript, python);
    }
}