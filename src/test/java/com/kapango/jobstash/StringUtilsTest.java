package com.kapango.jobstash;

import org.jsoup.Jsoup;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringUtilsTest {


    @Test
    public void cleansAndEscapesHtmlString() {

        final String input = "Poki — <a href=\"http:&#x2F;&#x2F;jobs.poki.com\" rel=\"nofollow\">http:&#x2F;&#x2F;jobs.poki.com</a> | Amsterdam | Onsite | Full-Time<p>Poki is an online playground with 30 million users around the world. With a team of 25 we build a web game platform that helps game developers achieve success, and brings fun games to kids of all ages around the world.<p>We’re a bootstrapped company where development, data and design come together.<p>We are looking for:<p>• Senior Product Designer - <a href=\"https:&#x2F;&#x2F;jobs.poki.com&#x2F;senior-product-designer&#x2F;en\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.poki.com&#x2F;senior-product-designer&#x2F;en</a><p>• Medior&#x2F;Senior Software Engineer - <a href=\"https:&#x2F;&#x2F;jobs.poki.com&#x2F;medior-senior-software-engineer&#x2F;en\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.poki.com&#x2F;medior-senior-software-engineer&#x2F;en</a><p>• Senior Front-end Developer - <a href=\"https:&#x2F;&#x2F;jobs.poki.com&#x2F;senior-front-end-developer-1&#x2F;en\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.poki.com&#x2F;senior-front-end-developer-1&#x2F;en</a><p>• Game Production &amp; QA Intern - <a href=\"https:&#x2F;&#x2F;jobs.poki.com&#x2F;game-production-qa-intern&#x2F;en\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.poki.com&#x2F;game-production-qa-intern&#x2F;en</a><p>#Stack: Go, Node, React, Redux, Kubernetes, Docker, Microservices, Prometheus, Google Cloud Platform.\n" +
                "We believe in giving smart and creative people the freedom and autonomy to do great work.<p>Apply: <a href=\"http:&#x2F;&#x2F;jobs.poki.com\" rel=\"nofollow\">http:&#x2F;&#x2F;jobs.poki.com</a>\n" +
                "Engineering &amp; Culture: <a href=\"http:&#x2F;&#x2F;blog.poki.com\" rel=\"nofollow\">http:&#x2F;&#x2F;blog.poki.com</a>\n" +
                "Website: <a href=\"http:&#x2F;&#x2F;poki.com&#x2F;\" rel=\"nofollow\">http:&#x2F;&#x2F;poki.com&#x2F;</a>";


        final String expected = "Poki — http://jobs.poki.com | Amsterdam | Onsite | Full-Time Poki is an online playground with 30 million users around the world. With a team of 25 we build a web game platform that helps game developers achieve success, and brings fun games to kids of all ages around the world. We’re a bootstrapped company where development, data and design come together. We are looking for: • Senior Product Designer - https://jobs.poki.com/senior-product-designer/en • Medior/Senior Software Engineer - https://jobs.poki.com/medior-senior-software-engineer/en • Senior Front-end Developer - https://jobs.poki.com/senior-front-end-developer-1/en • Game Production & QA Intern - https://jobs.poki.com/game-production-qa-intern/en #Stack: Go, Node, React, Redux, Kubernetes, Docker, Microservices, Prometheus, Google Cloud Platform. We believe in giving smart and creative people the freedom and autonomy to do great work. Apply: http://jobs.poki.com Engineering & Culture: http://blog.poki.com Website: http://poki.com/";

        final String result = Jsoup.parse(input).text();

        assertEquals(expected, result);

    }
}
